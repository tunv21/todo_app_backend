<?php
/**
 * @SWG\Swagger(
 *     schemes={"http", "https"},
 *     host=API_HOST,
 *     @SWG\Info(
 *         version="1.0.0",
 *         title="Laravel and Swagger",
 *         description="Getting started with Laravel and Swagger",
 *         description="name@example.com",
 *         termsOfService="",
 *         @SWG\Contact(
 *             email="name@example.com"
 *         ),
 *     ),
 * )
 */